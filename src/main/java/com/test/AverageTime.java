package com.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.test.domain.LogDetail;
import com.test.domain.LogRespnse;
import com.test.util.Utilities;

public class AverageTime {

	
	/**
	 * Reads File path and limit returns top records as per limit passed
	 * @param filePath path of the file
	 * @param limit for top records 
	 * @return list of urls
	 */
	public List<String> getUrlWithMostProcessingTime(String filePath, int limit){
		try (Stream<String> stream = Files.lines(Paths.get(filePath))) { // as this code is implementing closable interface no need to close file operation explicitly
			
			return  reduceByUrl(stream)
						.entrySet().stream() // stream of entry set
						.sorted( sortByAverageTimeinDesc()) // sort by average time
						.limit(limit) // number of records to be limited in reverse order
						.map(entry -> entry.getKey())  // map entry object to url string
						.collect(Collectors.toList()); // collect to list
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException("Exception is thrown while reading file");
		}
	}
	
	/**
	 * Reads lineStream and reduce the stream to be grouped by unique url and values will be LogRespnse
	 * Filtered out unnecessary log entries.
	 * Result will be having only those records satisfying below conditions
	 *  a) Status code is 200
	 *  b) url is not ending with gif
	 *  c) time format is in proper order 
	 * LogResponse will have averageTime for urls and count for how many time url logged in the log file
	 * Also as per problem statement url used as key is case insensitive. 
	 * @param lineStream
	 * @return Map<String,LogRespnse> url as key and LogRespnse as value
	 */
	private Map<String,LogRespnse> reduceByUrl(Stream<String> lineStream){
		
		return lineStream.map(logDetailMapper())	// lambda to map line in log file to LogDetailMapper	
				.filter(log->log.getStatusCode()==200)  // check for status code is 200
				.filter(log ->!log.getUrl().endsWith(".gif")) // check if url is for gif file
				.filter(log -> log.getResponseTime() !=-1) // response time is set as -1 is format is not in desired order
				.collect(Collectors.toMap(keyMapper(), valueMapper(), mergeFunction())); // to reduce the stream to map group by url
	}
	
	/**
	 * lambda to map url as key
	 * @return lambda to used by collector
	 */
	private Function<LogDetail, String> keyMapper(){
		return logDetails -> logDetails.getUrl();
	}
	
	/**
	 * lambda to map Logdetail to LogResponse
	 * @return lambda to used by collector
	 */
	private Function<LogDetail, LogRespnse> valueMapper(){
		return logDetails ->new LogRespnse(logDetails.getResponseTime(), 1) ;
	}
	
	/**
	 * lambda function to calculate average of response time 
	 * based on the number of elements processed by the stream
	 * 
	 * Formula used  averageSoFar* "number_of_time_url_found_before_this" + response_time_of_current_line) / 
	 * 																			number_of_time_url_found_soFar  
	 * @return lambda to be processed by collector
	 */
	private BinaryOperator<LogRespnse> mergeFunction(){
		 return (oldValue, newValue) -> {
			oldValue.setAverageTime(Utilities.getAverage(oldValue, newValue));
			oldValue.setCount(oldValue.getCount()+1);				
			return oldValue;
		};  
	}
	
	/**
	 * Return lambda to map String to Log details
	 * @return Function<String, LogDetail>
	 */
	private Function<String, LogDetail> logDetailMapper(){
		return log -> buildLogDetail(log);
	}
	
	/**
	 * lambda to fetch average time from map entry and do comparison based on average time
	 * To sort in reverse order we passed second argument first in Double.compare method 
	 * @return Comparator<Map.Entry<String, LogRespnse>> lambda function
	 */
	private Comparator<Map.Entry<String, LogRespnse>> sortByAverageTimeinDesc(){
		return (o1,o2) ->Double.compare(o2.getValue().getAverageTime() , o1.getValue().getAverageTime());
	}
	
	/**
	 * Build LogDeatil Object from line in a log file
	 * As per problem statement assumption is made that data is always in desired format 
	 * Foreg. /articles.html?id=4, 0.24s, Status Code: 200
	 * @param line line in a logfile
	 * @return
	 */
	private LogDetail buildLogDetail(String line) {
		String logFragments[] = line.split(",");	// splits line in a log based on comma
		/**
		 * Not checking size of logFragments array as per problem statement 
		 *  data always available in desired format
		 * First element contains url and we are converting everything to lower case 
		 *  as per statement url should be treated as case insensitive
		 */ 
		String url = logFragments[0].toLowerCase().trim();  
		int responseTime = Utilities.changeTimeFormat(logFragments[1]);  // second element contains responseTime
		int statusCode= Utilities.extractStatusCode(logFragments[2]);  // third element contains status code
	
		return new LogDetail(url,responseTime,statusCode);
	}
}
