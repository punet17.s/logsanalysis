package com.test.domain;

public class LogRespnse {
	private double averageTime;
	private int count;
	
	public LogRespnse(long averageTime, int count) {
		super();
		this.averageTime = averageTime;
		this.count = count;
	}
	public double getAverageTime() {
		return averageTime;
	}
	public void setAverageTime(double responseTime) {
		this.averageTime = responseTime;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "LogRespnse [responseTime=" + averageTime + ", count=" + count + "]";
	}
	
}
