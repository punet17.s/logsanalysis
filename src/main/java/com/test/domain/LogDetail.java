package com.test.domain;

public final class LogDetail {
	private String url;
	private int responseTime;
	private int statusCode;
	
	public LogDetail(String url, int responseTime, int statusCode) {
		super();
		this.url = url;
		this.responseTime = responseTime;
		this.statusCode = statusCode;
	}
	public String getUrl() {
		return url;
	}
	
	public int getResponseTime() {
		return responseTime;
	}
	public int getStatusCode() {
		return statusCode;
	}
	@Override
	public String toString() {
		return "LogDetail [url=" + url + ", responseTime=" + responseTime + ", statusCode=" + statusCode + "]";
	}
	
}
