package com.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.test.util.Utilities;


public class MainClass {
	public static void main(String[] args) throws IOException {
		AverageTime averageTime = new AverageTime();
		String filePath = "";
		
		if(args.length >=1) {
			filePath= args[0];
		}else {
			System.out.println("ERROR !! FilePath must be passed in command line arguments");
			System.exit(1);
		}
		
		int limit =args.length>1 && Utilities.isInteger(args[1])?Integer.parseInt(args[1]) : 10; 

		System.out.println("Fetching logs from file :: "+filePath +" for top "+ limit+" urls" );
		
		List<String> urls= averageTime.getUrlWithMostProcessingTime(filePath, limit);	
		
		System.out.println("Genrating reports .......");
		
		 
		Path path =Files.write(Paths.get("analysis.log"), urls);
		
		System.out.println("Report Genrated successfully in curent working directory ");

	}
}
