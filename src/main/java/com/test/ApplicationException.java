package com.test;

public class ApplicationException extends RuntimeException{

	public ApplicationException(String message) {
		super(message);
	}
	
}
