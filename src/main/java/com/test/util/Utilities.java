package com.test.util;


import com.test.domain.LogRespnse;

public class Utilities {
	/**
	 * Change responseTime in secs to mili secs
	 * Foreg. input is 100.23sec out put 123 int value 
	 * @param responseTime time in sec
	 * @return int value in milisec
	 */
	public static int changeTimeFormat(String responseTime) {
		String timeFragments[] = responseTime.trim().substring(0, responseTime.lastIndexOf("s") - 1).split("\\.");
		if (timeFragments.length == 2 && isInteger(timeFragments[0]) && isInteger(timeFragments[1])) {
			return Integer.parseInt(timeFragments[0]) * 100 + Integer.parseInt(timeFragments[1]);
		}
		return -1;
	}
	/**
	 * Check if value passed is a integer or not using regex
	 * @param intValue value to be checked if integer or not
	 * @return boolean
	 */
	public static boolean isInteger(String intValue) {
		return intValue.matches("^\\d+$"); // regex to check if input string is a number
	}
	
	/**
	 * Extract status code int value from the string 
	 * Foreg. Status Code: 200 will return 200 
	 * @param status String in the log file 
	 * @return int as statusCode intValue
	 */
	public static int extractStatusCode(String status) {

		String statusCodeFragments[] = status.split(":");
		if (statusCodeFragments.length == 2 && isInteger(statusCodeFragments[1].trim())) {
			return Integer.parseInt(statusCodeFragments[1].trim());
		}
		return -1;
	}
	
	/**
	 * Get Average of Stream of data based on the number of records processed so far
	 * @param entry1 existing entry in map
	 * @param entry2 new Entry found in stream against a key
	 * @return average value so far
	 */
	public static double getAverage(LogRespnse entry1,LogRespnse entry2 ) {
		double averageSoFar = entry1.getAverageTime(); // average before new Entry is processed in a stream
		return (averageSoFar*entry1.getCount() + entry2.getAverageTime())/(entry1.getCount()+1);
		
	}
}
