package com.test;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;
import org.hamcrest.collection.IsEmptyCollection;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

public class AverageTimeTest {
	AverageTime averageTime = new AverageTime();
	
	/**
	 * 
	 * Positive case with all valid inputs  
	 *  
	 */
	@Test
	public void testGetUrlWithMostProcessingTime() {
		List<String> expectedResult = Arrays.asList("/articles.html?id=8","/articles.html?id=2","/articles.css",
										"/rticles.html?id=4","/articles","/articles.html?id=1",
										"/articles.html?id=3","/articles.html?id=4","/articles.html?id=10",
										"/articles.html?id=9") ;
		List<String> actualResult= 
			  averageTime.getUrlWithMostProcessingTime(getBasePath().resolve("nginix.log").toString(), 10);
		assertThat(actualResult, is(expectedResult));
	}
	/**
	 * if Logfile is empty it should return empty arrayList
	 * 
	 */
	@Test
	public void testGetUrlWithMostProcessingTime_hasEmpty_LogFile() {
		List<String> actualResult= 
			  averageTime.getUrlWithMostProcessingTime(getBasePath().resolve("nginix_empty.log").toString(), 10);
		assertThat(actualResult, IsEmptyCollection.empty());
	}
	
	/**
	 * If logFile has less than 10 unique records,
	 * In this case total 4 results are returned 
	 * 
	 */
	@Test
	public void testGetUrlWithMostProcessingTime_has4DistinctRecords_LogFile() {
		List<String> expectedResult = Arrays.asList("/articles.html?id=1","/articles.html?id=3","/articles.html?id=4","/articles.css") ;
		List<String> actualResult= 
			  averageTime.getUrlWithMostProcessingTime(getBasePath().resolve("nginix_four_distinctRecord.log").toString(), 10);
		assertThat(actualResult,is(expectedResult));
		assertThat(actualResult, hasSize(4));
	}
	/**
	 * If .gif is present in logs then code should ignore 
	 * 
	 */
	@Test
	public void testGetUrlWithMostProcessingTime_ignore_GifFile() {
		List<String> expectedResult = Arrays.asList("/articles.html?id=1","/articles.html?id=3","/articles.html?id=4","/articles.css") ;
		List<String> actualResult= 
			  averageTime.getUrlWithMostProcessingTime(getBasePath().resolve("nginix_ignore_gif.log").toString(), 10);
		assertThat(actualResult, not(hasItem("/articles.gif")));
		assertThat(actualResult,is(expectedResult));
		assertThat(actualResult, hasSize(4));
	}
	
	/**
	 * If status code is other than 200 in logs then code should ignore
	 * 
	 */
	@Test
	public void testGetUrlWithMostProcessingTime_ignore_otherThan_200() {
		List<String> expectedResult = Arrays.asList("/articles.html?id=3","/articles.html?id=4","/articles.css") ;
		List<String> actualResult= 
			  averageTime.getUrlWithMostProcessingTime(getBasePath().resolve("nginix_ignore_otherThan_200.log").toString(), 10);
		assertThat(actualResult,is(expectedResult));
		assertThat(actualResult, not(hasItem("/articles.html?id=1")));
		assertThat(actualResult, hasSize(3));
	}
	/**
	 * If status code is other than 200 in logs then code should ignore
	 * 
	 */
	@Test(expected=ApplicationException.class)
	public void testGetUrlWithMostProcessingTime_throw_applicationException_fileNotFound() { 
			  averageTime.getUrlWithMostProcessingTime(getBasePath().resolve("nginix_file_not_found.log").toString(), 10);

	}
	
	public Path getBasePath() {
		return Paths.get("").resolve("src").resolve("test").resolve("resources");
	}
}
